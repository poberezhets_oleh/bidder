'use strict'

exports.sspFilter = (sspId, targetCode, isAllowedTarget) => {
    const sspCode = parseInt('1' + '0'.repeat(sspId - 2) + '1' + '0'.repeat(targetCode.length - sspId) , 2);
    return sspCode & targetCode && isAllowedTarget
};
