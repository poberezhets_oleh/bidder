'use strict';

const http = require('http');
const path = require('path');
const url = require('url');
const cuid = require('cuid');

const routerLoader = require('./Service/RouterLoader');
const OrdersManagementService = require('./Service/Bidder/OrdersManagementService');
const SspPartnersManagementService = require('./Service/Bidder/SspPartnersManagementService');
const BidRequestProcessor = require('./Service/Bidder/BidRequestProcessor');
const AuctionService = require('./Service/Bidder/AuctionService');

const ExtendableError = require('./Error/ExtendableError');

const IMP_TYPE_BANNER = 0;
const IMP_TYPE_VIDEO  = 1;
const IMP_TYPE_NATIVE = 2;

const BANNER_BID_TTL = 6000;
const VIDEO_BID_TTL  = 3600;
const NATIVE_BID_TTL = 600;

const MAINTENANCE_MESSAGE = 'MAINTENANCE';
const ORDERS_LIST_MESSAGE = 'ORDERS_LIST';
const PARTNERS_LIST_MESSAGE = 'PARTNERS_LIST';

class Bidder {
    constructor(config, logger, cacheClient, storageClient, geoLocationService) {
        this._config = config;
        this._logger = logger;
        this._cacheClient = cacheClient;
        this._storageClient = storageClient;
        this._geoLocationService = geoLocationService;
        this._ordersManagementService = new OrdersManagementService();
        this._sspPartnersService = new SspPartnersManagementService();
        this._bidRequestProcessor = new BidRequestProcessor(config, geoLocationService, cacheClient);
        this._auctionService = new AuctionService(config, logger);

        this._markup = 0.2;

        this._routes = null;
        this._httpServer = null;
        this._netAddress = null;

        this._active = false;
        this._maintenanceMode = false;

        this._storageClient.on('error', error => {
           this._logger.error(error.message, error);
        });
    }

    get CLASS_NAME() {
        return this.constructor.name;
    }

    getLogger() {
        return this._logger;
    }

    async bootstrap() {
        this._routes = await routerLoader(path.resolve(__dirname, 'Routes/Bidder'), this);

        this._netAddress = await this._getNetworkAddress();

        // await this._geoLocationService.startService();
    }

    start() {
        this._httpServer = http.createServer((req, res) => {
            this._httpHandler(req, res)
                .catch(err => {
                    this._logger.error(err.message, err);

                    if (!res.headersSent) {
                        res.statusCode = 204;
                        res.end();
                    }
                });
        });

        this._httpServer.listen(this._config.httpPort, this._netAddress);
    }

    stop() {
        return new Promise((resolve, reject) => {
            this._httpServer.close(resolve);
        })
    }

    /**
     * @param {Object} message
     * @param {string} message.type
     * @param {*} message.data
     */
    processMessage(message) {
        if (message.type === MAINTENANCE_MESSAGE) {
            this._maintenanceMode = message.data.enable;
            this._ordersManagementService.setOrders([]);
        }

        if (message.type === ORDERS_LIST_MESSAGE) {
            this._ordersManagementService.setOrders(message.data);
        }

        if (message.type === PARTNERS_LIST_MESSAGE) {
            this._sspPartnersService.setPartners(message.data);
        }
    }

    async processBidRequest(query, bidRequest) {
        if (this._maintenanceMode) {
            return;
        }

        const ssp = this._sspPartnersService.getSspPartner(query.accessKey);

        if (ssp === undefined) {
            throw new ExtendableError('Can`t find SSP partner', {accessKey: query.accessKey});
        }

        if (!BidRequestProcessor.isValidBidRequest(bidRequest)) {
            throw new ExtendableError('Wrong top-level bid request object structure', bidRequest);
        }

        const bidsMap = Object.create(null);
        const impPriceMap = Object.create(null);

        for (const impObject of bidRequest.imp) {
            if (!BidRequestProcessor.isValidImpObject(impObject)) {
                continue;
            }

            const impType = BidRequestProcessor.getImpressionType(impObject);

            if (impType === IMP_TYPE_BANNER) {
                const bannerBids = this._ordersManagementService.getBannerBids();

                if (bannerBids.length === 0) {
                    continue;
                }

                const orders = this._bidRequestProcessor.findAcceptableBids('banner', impObject, bannerBids);

                if (orders.length === 0) {
                    continue;
                }

                impPriceMap[impObject.id] = impObject.bidfloor;
                bidsMap[impObject.id] = orders;
            } else {
                // TODO process all impressions type
                return null;
            }
        }

        if (Object.keys(bidsMap).length === 0 ) {
            return;
        }

        let bidsByImpId = Object.create(null);
        let inventoryInfo = Object.create(null);
        let deviceInfo = Object.create(null);
        let geoInfo = Object.create(null);

        try {
            ({bidsByImpId, inventoryInfo, deviceInfo, geoInfo} =
                this._bidRequestProcessor.filtersAndOrderBidInfo(bidRequest, ssp.id, bidsMap));
        } catch (err) {
            this._logger.error(err.message, err);

            return;
        }

        if (Object.keys(bidsByImpId).length === 0 ) {
            return;
        }

        const seatBids = [];
        const promises = [];

        for (const impId in bidsByImpId) {
            const winBid = this._auctionService.conductAuction(bidsByImpId[impId]);

            const id = cuid();
            const sspPrice = impPriceMap[impId];
            const cridPrice = winBid.li_max_price || winBid.c_max_price;
            const bidPrice = +(cridPrice * (1 - this._markup)).toFixed(5);

            const creativeClickUrl = winBid.adomain
                .replace(/\{AUCTION_ID\}/g, bidRequest.id)
                .replace(/\{AUCTION_BID_ID\}/g, bidRequest.id)
                .replace(/\{AUCTION_SEAT_ID\}/g, bidRequest.adv_id)
                .replace(/\{AUCTION_AD_ID\}/g, impId)
                .replace(/\{AUCTION_CREATIVE_ID\}/g, winBid.cr_id)
                .replace(/\{AUCTION_CAMPAIGN_ID\}/g, winBid.c_id);

            const impUrl = this._composeImpUrl(id, ssp.id);
            const dspClickUrl = this._composeClickUrl(id);
            const admCode = this._composeBannerAdm(id, winBid.primary_asset, creativeClickUrl, impUrl, dspClickUrl);

            const bidInfo = {
                creative: winBid.cr_id,
                line_item: winBid.li_id,
                campaign: winBid.c_id,
                advertiser: winBid.adv_id,
                ssp: ssp.id,
                ssp_price: sspPrice,
                bid_price: bidPrice,
                crid_price: cridPrice,
                country: geoInfo.country || null,
                region: geoInfo.region || null,
                city: geoInfo.city || null,
                placement: inventoryInfo.type || null,
                placement_id: inventoryInfo.id || null,
                domain: inventoryInfo.domain || null,
                app_name: inventoryInfo.name || null,
                bundle: inventoryInfo.bundle || null,
                browser: deviceInfo.browser || null,
                ip: deviceInfo.ip,
                os: deviceInfo.os || null,
                isp: deviceInfo.isp || null,
                connection: deviceInfo.connection,
                lang: deviceInfo.lang || null,
                device: deviceInfo.device,
                brand: deviceInfo.brand || null,
                model: deviceInfo.model || null,
            };

            const cacheData = Object.assign({
                id,
                markup: this._markup,
                ifa: deviceInfo.ifa,
                test: bidRequest.test || 0
            }, bidInfo);

            await this._cacheClient.setBid(id, cacheData, BANNER_BID_TTL);
            this._storageClient.saveRecord(bidInfo);

            seatBids.push({
                seat: winBid.adv_id,
                group: 0,
                bid: {
                    id: id,
                    impid: impId,
                    price: bidPrice,
                    nurl: this._composeNurl(id, ssp.id),
                    adm: admCode,
                    adid: id,
                    adomain: [winBid.adomain],
                    cid: winBid.c_id.toString(),
                    crid: winBid.cr_id.toString(),
                    cat: winBid.cat,
                }
            });
        }

        return {
            id: bidRequest.id,
            seatbid: seatBids,
            cur: 'USD'
        };
    }

    _composeNurl(bidId, sspId) {
        return `${this._config.bookkeeperURI}/win?auction_id=${bidId}&ssp_id=${sspId}&win_price=` + '${AUCTION_PRICE}';
    }

    _composeImpUrl(bidId, sspId) {
        return `${this._config.bookkeeperURI}/impression?auction_id=${bidId}&ssp_id=${sspId}&win_price=`
            + '${AUCTION_PRICE}';
    }

    _composeClickUrl(bidId) {
        return `${this._config.bookkeeperURI}/click?auction_id=${bidId}`;
    }

    _composeBannerAdm(bidId, asset, assetClickUrl, impressionUri, clickUri) {
        const admCode =
            `<span id='${bidId}'><img src='${impressionUri}' width='0' height='0'><a href='${assetClickUrl}'>` +
            `<img src='${asset}'></a></span>` +
            "<script type='application/javascript'>" +
            "(function () {var wasClick=false;var isActive=false;function onMouseOut(){isActive = false;" +
            "window.top.focus();}function onMouseOver(){isActive = true;}function onClick(ev){if(!wasClick){" +
            "wasClick=true;return;}var href;" +
            `const aTag=document.getElementById('${bidId}').getElementsByTagName('a')[0];` +
            "if (aTag!==undefined){href=aTag.href;}if(aTag.target!=='_blank'){ev.preventDefault();}" +
            `var xhr = new XMLHttpRequest();xhr.open('GET','${clickUri}',true);xhr.onreadystatechange=function(){` +
            "if(xhr.readyState===XMLHttpRequest.DONE&&href&&aTag.target!=='_blank'){document.location = href;" +
            "}};xhr.send(null);}function pfc(){if(isActive){onClick();}}" +
            `var element=document.getElementById('${bidId}');element.onclick = onClick;` +
            "element.onmouseover=onMouseOver;element.onmouseout = onMouseOut;if(window.attachEvent!==undefined){" +
            "window.top.attachEvent('onblur', pfc);}else if(window.addEventListener!==undefined){" +
            "window.top.addEventListener('blur', pfc, false);}})();" +
            "</script>";

        return admCode;
    }

    /**
     * @returns {Promise.<string>}
     * @private
     */
    async _getNetworkAddress() {
        if (process.env.BIDDER_LAN_IP !== undefined) {
            return process.env.BIDDER_LAN_IP;
        } else {
            return '0.0.0.0';
        }
    }

    async _httpHandler(req, res) {
        const {headers, method, url: reqUrl} = req;
        const {pathname, query} = url.parse(reqUrl, true);

        const routePath = pathname.replace(/\/$/, ''); // delete terminal slash

        if (this._routes[routePath] !== undefined) {
            if (this._routes[routePath][method] !== undefined) {
                if (method === 'POST') {
                    let body = [];

                    req.on('data', chunk => {
                        body.push(chunk);
                    });

                    req.on('end', () => {
                        const bodyString = Buffer.concat(body).toString('utf8'); //convert to string

                        return this._routes[routePath][method](res, query, bodyString, headers);
                    });
                } else {
                    return this._routes[routePath][method](res, query, req.headers);
                }
            } else {
                this._logger.warn(`${this.CLASS_NAME}::_httpHandler(): Not supported HTTP method`, {method, reqUrl});

                res.statusCode = 404;
                res.end();
            }
        } else {
            this._logger.warn(`${this.CLASS_NAME}::_httpHandler(): Not supported route`, {method, reqUrl});

            res.statusCode = 404;
            res.end();
        }
    }
}

module.exports = Bidder;
