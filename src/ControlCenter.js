'use strict';

const url = require('url');
const http = require('http');
const path = require('path');

const ConsulAgent = require('consul-agent');
const SystemHealthMonitor = require('system-health-monitor');

const ServiceNotStarted = require('./Error/ServiceNotStarted');
const routerLoader = require('./Service/RouterLoader');
const OrdersManagementService = require('./Service/ControlCenter/OrdersManagementService');
const SspManagementService = require('./Service/ControlCenter/SspManagementService');

const MAINTENANCE_MESSAGE = 'MAINTENANCE';
const ORDERS_LIST_MESSAGE = 'ORDERS_LIST';
const PARTNERS_LIST_MESSAGE = 'PARTNERS_LIST';

class ControlCenter {
    constructor(config, logger, consul) {
        this._config = config;
        this._logger = logger;
        this._consul = consul;

        this._httpPort = config.httpPort;
        this._pid = process.pid;

        this._routes = null;
        this._httpServer = null;
        this._netAddress = null;
        this._serviceId = null;

        this._consulAgent = new ConsulAgent(this._consul);

        this._systemHealthMonitor = null;
        this._ordersManagementService = new OrdersManagementService(config.ordersService);
        this._sspManagementService = new SspManagementService(config.sspService);

        this._active = false;
        this._maintenanceMode = false;
    }

    get CLASS_NAME() {
        return this.constructor.name;
    }

    async bootstrap() {
        this._routes = await routerLoader(path.resolve(__dirname, 'Routes/ControlCenter'), this);
        this._netAddress = await this._getNetworkAddress();

        const healthCheckUri = `http://${this._netAddress}:${this._httpPort}/v1/service/status`;
        this._serviceId = `${this._netAddress}_${this._httpPort}_bidder`;

        this._consulAgent.setServiceAddress(this._netAddress);
        this._consulAgent.setServicePort(this._httpPort);
        this._consulAgent.setServiceId(this._serviceId);
        this._consulAgent.setServiceName(this._config.serviceName);
        this._consulAgent.addCheck(healthCheckUri, `${this._config.healthCheck.interval}s`);
    }

    async start() {
        this._systemHealthMonitor = new SystemHealthMonitor(this._config.systemHealthMonitor);
        await this._systemHealthMonitor.start();

        this._ordersManagementService.on(OrdersManagementService.CHANGED_EVENT, ordersList => {
            process.send({
               type: ORDERS_LIST_MESSAGE,
               data: ordersList
           });
        });

        this._ordersManagementService.on(OrdersManagementService.UNHEALTHY_EVENT, () => {
           process.send({
               type: ORDERS_LIST_MESSAGE,
               data: []
           });
        });

        this._ordersManagementService.on(OrdersManagementService.ERROR_EVENT, error => {
            this._logger.error(error.message, error);

            process.send({
                type: ORDERS_LIST_MESSAGE,
                data: []
            });
        });

        await this._ordersManagementService.startService();

        process.send({
            type: ORDERS_LIST_MESSAGE,
            data: this._ordersManagementService.getBids()
        });

        this._sspManagementService.on(SspManagementService.CHANGED_EVENT, sspList => {
           process.send({
               type: PARTNERS_LIST_MESSAGE,
               data: sspList
           });
        });

        this._sspManagementService.on(SspManagementService.UNHEALTHY_EVENT, () => {
           process.send({
               type: PARTNERS_LIST_MESSAGE,
               data: []
           });
        });

        this._sspManagementService.on(SspManagementService.ERROR_EVENT, error => {
            this._logger.error(error.message, error);

            process.send({
                type: PARTNERS_LIST_MESSAGE,
                data: []
            });
        });

        await this._sspManagementService.startService();

        process.send({
            type: PARTNERS_LIST_MESSAGE,
            data: this._sspManagementService.getSspPartners()
        });

        await this._consulAgent.registerService(true);

        this._httpServer = http.createServer((req, res) => {
            this._httpHandler(req, res)
                .catch(err => {
                    this._logger.error(err.message, err);
                });
        });

        this._httpServer.listen(this._config.httpPort, this._netAddress);

        this._active = true;
    }

    async stop() {
        if (!this._active) return;

        await this._consulAgent.deregisterService();

        this._ordersManagementService.stopService();
        this._sspManagementService.stopService();

        this._httpServer.close();

        this._active = false;
    }

    /** @returns {boolean} */
    isMaintenanceMode() {
        return this._maintenanceMode;
    }

    enableMaintenanceMode() {
        this._maintenanceMode = true;

        process.send({
            type: MAINTENANCE_MESSAGE,
            data: {
                enable: true
            }
        });
    }

    disableMaintenanceMode() {
        this._maintenanceMode = false;

        process.send({
            type: MAINTENANCE_MESSAGE,
            data: {
                enable: false
            }
        });
    }

    getSystemHealthMonitorInfo() {
        if (!this._active) {
            throw new ServiceNotStarted();
        }

        const info = {
            status: 'OK',
            pid: this._pid,
            mem:    {
                total: this._systemHealthMonitor.getMemTotal(),
                free:  this._systemHealthMonitor.getMemFree()
            },
            cpu:    {
                usage: this._systemHealthMonitor.getCpuUsage(),
                count: this._systemHealthMonitor.getCpuCount()
            }
        };

        if (this._systemHealthMonitor.isOverloaded()) {
            info.status = 'OVERLOADED';
        }

        if (this._maintenanceMode) {
            info.status = 'MAINTENANCE';
        }

        return info;
    }

    /**
     * @returns {Promise.<string>}
     * @private
     */
    async _getNetworkAddress() {
        if (process.env.BIDDER_LAN_IP !== undefined) {
            return process.env.BIDDER_LAN_IP;
        } else {
            const result = await this._consulAgent.getNetAddresses();

            return result.lan;
        }
    }

    async _httpHandler(req, res) {
        const {headers, method, url: reqUrl} = req;
        const {pathname, query} = url.parse(reqUrl, true);

        let ip = headers['x-forwarded-for'] && headers['x-forwarded-for'].split(',').pop()
            || req.connection.remoteAddress
            || req.socket.remoteAddress;

        if (ip.startsWith('::ffff:')) {
            ip = ip.substr(7);
        }

        const routePath = pathname.replace(/\/$/, ''); // delete terminal slash

        if (this._routes[routePath] !== undefined) {
            if (this._routes[routePath][method] !== undefined) {
                if (method === 'POST') {
                    let body = [];

                    req.on('data', (chunk) => {
                        body.push(chunk);
                    });

                    req.on('end', () => {
                        let data;

                        const bodyString = Buffer.concat(body).toString(); //convert to string

                        try {
                            data = JSON.parse(bodyString);
                        } catch (err) {
                            this._logger.error(err.message, err);
                        }

                        return this._routes[routePath][method](res, query, data, headers);
                    });
                }

                return this._routes[routePath][method](res, query);
            } else {
                this._logger.warn(`${this.CLASS_NAME}::_httpHandler(): Not supported HTTP method`, {ip, method, reqUrl});

                res.statusCode = 404;
                res.end();
            }
        } else {
            this._logger.warn(`${this.CLASS_NAME}::_httpHandler(): Not supported route`, {ip, method, reqUrl});

            res.statusCode = 404;
            res.end();
        }
    }
}

module.exports = ControlCenter;
