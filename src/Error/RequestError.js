'use strict';
const ExtendableError = require('./ExtendableError');

class RequestError extends ExtendableError {
    /**
     * @param {Error} error
     * @param {*} meta
     */
    constructor(error, meta) {
        super(`Request error: ${error.message}`, {error, meta});
        this.code = 'ERR_REQUEST';
        this.name = this.constructor.name;

        // noinspection JSUnresolvedFunction
        Error.captureStackTrace(this, this.constructor);
    }

    /**
     * @return {string}
     */
    toString() {
        return !this.extra
            ? super.toString()
            : super.toString() + ' Extra: ' + JSON.stringify(this.extra);
    }
}

module.exports = ExtendableError;
