'use strict';
const ExtendableError = require('./ExtendableError');

class ServiceNotStarted extends ExtendableError {
    /**
     * @param {{}} [meta]
     */
    constructor(meta) {
        super('Service not started', meta);
        this.code = 'ERR_NOT_STARTED';
        this.name = this.constructor.name;

        // noinspection JSUnresolvedFunction
        Error.captureStackTrace(this, this.constructor);
    }

    /**
     * @return {string}
     */
    toString() {
        return !this.extra
            ? super.toString()
            : super.toString() + ' Extra: ' + JSON.stringify(this.extra);
    }
}

module.exports = ServiceNotStarted;
