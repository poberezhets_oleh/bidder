'use strict';

class ConsulAgent {
    /**
     * @param {Consul} consul
     */
    constructor(consul) {
        this._consul = consul;
        this._name = undefined;
        this._id = undefined;
        this._tags = [];
        this._address = undefined;
        this._port = undefined;
        this._checks = [];

        this._serviceRegistered = false;
    }

    setServiceName(name) {
        this._name = name;
    }

    setServiceId(id) {
        this._id = id;
    }

    setServiceTags(tags) {
        if (Array.isArray(tags)) {
            this._tags = tags;
        }
    }

    setServiceAddress(address) {
        this._address = address;
    }

    setServicePort(port) {
        this._port = port;
    }

    addCheck(uri, interval) {
        this._checks.push({
            http: uri,
            interval
        });
    }

    /**
     * @return {Promise.<{lan: string, wan: string}>}
     */
    async getNetAddresses() {
        const result = await this._consul.agent.self();
        return result.DebugConfig.TaggedAddresses;
    }

    /**

     * @param {boolean} deregister
     * @returns {Promise.<void>}
     */
    async registerService(deregister) {
        if (deregister) {
            await this._consul.agent.service.deregister(this._id);
        }

        if (this._serviceRegistered) {
            return;
        }

        await this._consul.agent.service.register({
            name: this._name,
            id: this._id,
            tags: this._tags,
            address: this._address,
            port: this._port,
            checks: this._checks
        });

        this._serviceRegistered = true;
    }

    /**
     * @returns {Promise}
     */
    deregisterService() {
        return this._consul.agent.service.deregister(this._id);
    }
}

module.exports = ConsulAgent;
