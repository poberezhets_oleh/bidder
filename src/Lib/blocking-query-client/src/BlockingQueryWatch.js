'use strict';

const EventEmitter = require('events').EventEmitter;
const util = require('util');
const {lookup} = require('dns-lookup-cache');
const request = require('request-promise-native');
const _ = require('lodash');

/**
 * @emits BlockingQueryWatch#changed
 * @emits BlockingQueryWatch#error
 * @emits BlockingQueryWatch#healthy
 * @emits BlockingQueryWatch#unhealthy
 */
class BlockingQueryWatch extends EventEmitter {
    /**
     * @param {string} url
     * @param {number|undefined} wait
     */
    constructor(url, wait) {
        super();

        if (!_.isString(url) || _.isEmpty(url)) {
            throw new Error('url must be non-empty string');
        }

        if (!_.isUndefined(wait) && (!_.isSafeInteger(wait) || wait <= 0)) {
            throw new Error('wait must be number greater then zero if set');
        }

        this._url = url;
        this._wait = wait;

        this._started = false;
        this._healthy = false;
        this._retryAttempts = 0;
        this._request = undefined;
        this._updateDate = undefined;
        this._modifyIndex = undefined;
    }

    /**
     * Returns date of latest successful response from server.
     *
     * @returns {Date|undefined}
     */
    getUpdateDate() {
        return this._updateDate;
    }

    /**
     * Starts service and resolves promise with initial list of nodes that provide the service in case of success.
     *
     * Throws error if first request failed with any issues.
     * If so, any retry attempts do not performed, and watch instance should be considered as broken.
     *
     * @returns {Promise.<Object|undefined>}
     */
    async start() {
        if (this._started) {
            throw new Error('Watch already started');
        }

        const response = await this._run();

        if (_.isUndefined(response)) {
            this.stop();

            // First request is failed so assume that watch instance is incorrect and should not retry
            throw new Error('Watch is broken');
        } else {
            this._started = true;
            this._healthy = true;

            process.nextTick(() => this._run());

            return response;
        }
    }

    stop() {
        this._started = false;
        this._healthy = false;
        this._request.abort();

        this.emit('unhealthy');
    }

    /**
     * @returns {Promise<Object|undefined>}
     * @private
     */
    async _run() {
        const options = {
            url: this._url,
            method: 'GET',
            json: true,
            followRedirect: true,
            maxRedirects: 3,
            lookup: lookup,
            family: 4,
            forever: true,
            qs: {
                index: this._modifyIndex,
                wait: this._wait
            }
        };

        try {
            this._request = request(options);
            const response = await this._request;

            if (!this._healthy) {
                this._healthy = true;
                this.emit('healthy');
            }

            this._retryAttempts = 0;
            this._updateDate = new Date();

            const newIndex = response.updateTime;

            if (newIndex !== this._modifyIndex) {
                this._modifyIndex = newIndex;

                this.emit('changed', response);
            }

            if (this._started) {
                process.nextTick(() => this._run());
            }

            return response;
        } catch (error) {
            this.emit('error', error);

            if (this._healthy) {
                this._healthy = false;
                this.emit('unhealthy');
            }

            if (this._started) {
                this._modifyIndex = undefined; // Prevents stall in "unhealthy" state

                setTimeout(() => this._started && this._run(), this._generateWaitTime());
            }
        }
    }

    /**
     * @param {Object} response
     * @returns {boolean}
     * @private
     */
    _validateResponse(response) {
        return _.isString(response.updateTime);
    }

    /**
     * @returns {number} 200, 400, 800, ..., up to 25600, and max value repeated continuously
     * @private
     */
    _generateWaitTime() {
        return Math.min(Math.pow(2, ++this._retryAttempts), 256) * 100;
    }
}

module.exports = BlockingQueryWatch;
