'use strict';

module.exports = (app) => {
    return {
        '/v1/service/maintenance': {
            'GET': (res) => {
                return res.end(JSON.stringify({
                    data: app.isMaintenanceMode()
                }));
            },
            'POST': (res, query, data) => {
                if (data.enable === true) {
                    app.enableMaintenanceMode();
                } else if (data.enable === false) {
                    app.disableMaintenanceMode();
                }

                res.setHeader('Content-Type', 'application/json');
                return res.end(JSON.stringify({data: null}));
            }
        },
        '/v1/service/status': {
            'GET': (res) => {
                try {
                    const info = app.getSystemHealthMonitorInfo();

                    if (info.status === 'OK') {
                        res.setHeader('Content-Type', 'application/json');

                        return res.end(JSON.stringify({data: info}));
                    } else {
                        res.statusCode = 503;
                        res.setHeader('Content-Type', 'application/json');

                        return res.end(JSON.stringify({data: info}));
                    }
                } catch (err) {
                    console.log(err);

                    res.statusCode = 500;

                    return res.end();
                }
            }
        },
    };
};
