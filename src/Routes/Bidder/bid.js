'use strict';

module.exports = (app) => {
    const logger = app.getLogger();

    return {
        '/v1/bid': {
            'POST': async (res, query, bodyString, headers) => {
                let data;

                try {
                    if (headers['content-encoding'] === 'gzip') {
                        // TODO gzip
                        res.statusCode = 204;
                        return res.end();
                    }

                    if (headers['content-type'] === 'application/json') {
                        data = JSON.parse(bodyString);
                    }

                    const result = await app.processBidRequest(query, data);

                    if (result === null || result === undefined) {
                        res.statusCode = 204;
                        res.setHeader('Content-Type', 'application/json');
                        res.write(JSON.stringify({
                            nbr: 3
                        }));

                        res.end();

                        return;
                    }

                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.end(JSON.stringify(result));
                } catch (err) {
                    res.statusCode = 204;
                    res.end('No bid');
                    logger.error(err.message, err);
                }
            }
        }
    };
};
