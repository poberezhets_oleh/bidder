'use strict';

const EventEmitter = require('events');
const ExtendableError = require('../../Error/ExtendableError');
const {BlockingQueryWatcher} = require('../BlockingQueryWatcher');

/**
 * @emits SspManagementService#healthy
 * @emits SspManagementService#unhealthy
 * @emits SspManagementService#changed
 * @emits SspManagementService#error
 */
class SspManagementService extends EventEmitter {
    static get HEALTHY_EVENT() {
        return 'healthy';
    }

    static get UNHEALTHY_EVENT() {
        return 'unhealthy';
    }

    static get CHANGED_EVENT() {
        return 'changed';
    }

    static get ERROR_EVENT() {
        return 'error';
    }

    /**
     * @param {Object} config
     * @param {string} config.url
     * @param {number} config.wait
     */
    constructor(config) {
        super();

        this._onWatcherChange = this._onWatcherChange.bind(this);
        this._onWatcherError = this._onWatcherError.bind(this);
        this._onWatcherHealthy = this._onWatcherHealthy.bind(this);
        this._onWatcherUnhealthy = this._onWatcherUnhealthy.bind(this);

        this._bids = Object.create(null);

        this._blockingQueryWatcher = new BlockingQueryWatcher(config);
    }

    /**
     * @returns {string}
     */
    get CLASS_NAME() {
        return this.constructor.name;
    }

    getSspPartners() {
        return this._bids;
    }

    async startService() {
        try {
            const data = await this._blockingQueryWatcher.startService();

            this._bids = data.ssp;
        } catch (error) {
            throw new ExtendableError(`${this.CLASS_NAME} start was failed`, {error});
        }

        this._blockingQueryWatcher.on('changed', this._onWatcherChange);
        this._blockingQueryWatcher.on('error', this._onWatcherError);
        this._blockingQueryWatcher.on('healthy', this._onWatcherHealthy);
        this._blockingQueryWatcher.on('unhealthy', this._onWatcherUnhealthy);
    }

    /**
     * Stops service even if it is not started yet.
     *
     * @returns {SspManagementService}
     */
    stopService() {
        if (!this._isWatcherRegistered()) {
            return this;
        }

        this._blockingQueryWatcher.removeAllListeners();
        this._blockingQueryWatcher.stopService();
        this._blockingQueryWatcher = null;
        return this;
    }

    _isWatcherRegistered() {
        return this._blockingQueryWatcher !== null;
    }

    _onWatcherChange(data) {
        this._bids = data.ssp;

        this.emit(SspManagementService.CHANGED_EVENT, data.ssp);
    }

    _onWatcherError(err) {
        this.emit(SspManagementService.ERROR_EVENT, err);
    }

    _onWatcherHealthy() {
        this.emit(SspManagementService.HEALTHY_EVENT);
    }

    _onWatcherUnhealthy() {
        this.emit(SspManagementService.UNHEALTHY_EVENT);
    }
}

module.exports = SspManagementService;
