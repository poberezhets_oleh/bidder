'use strict';

const LoggerWithContext = require('./logger/LoggerWithContext');

class LoggerWithContextFactory {

    /**
     * @params {String} context
     * @params {winston.logger} logger
     * @return {LoggerWithContext}
     */
    create(context, logger) {
        return new LoggerWithContext(context, logger);
    }
}

module.exports = LoggerWithContextFactory;
