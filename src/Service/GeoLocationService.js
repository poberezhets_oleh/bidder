'use strict';

const maxmind = require('maxmind');

class GeoLocationService {
    constructor(config) {
        this._config = config;
        this._geoLocationLookup = null;

        this._started = false;
    }

    get CLASS_NAME() {
        return this.constructor.name;
    }

    startService() {
        return new Promise((resolve, reject) => {
            maxmind.open(
                this._config.geoLocationDbPath,
                {
                    watchForUpdates: true,
                    watchForUpdateNonPersistent: true
                },
                (err, geoLocationLookup) => {
                    if (err) {
                        return reject(err);
                    }

                    this._started = true;
                    this._geoLocationLookup = geoLocationLookup;

                    resolve();
            });
        });
    }

    /**
     * @param {string} ip
     * @returns {Object}
     */
    getLocation(ip) {
        if (!this._started) {
            throw new Error(`${this.CLASS_NAME} not started`);
        }

        return this._geoLocationLookup.get(ip);
    }
}

module.exports = GeoLocationService;
