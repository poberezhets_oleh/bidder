'use strict';

class AuctionService {
    constructor(config, logger) {
        this._config = config;
        this._logger = logger;
    }

    conductAuction(bids) {
        let bidsPriceSum = 0;

        for (const bid of bids) {
            bidsPriceSum += (bid.li_max_price || bid.c_max_price);
        }

        const randPoint = Math.random() * bidsPriceSum;

        let boundaryPoint = 0;
        for (const bid of bids) {
            boundaryPoint += bid.li_max_price || bid.c_max_price;
            if (randPoint < boundaryPoint) {
                return bid;
            }
        }
    }
}

module.exports = AuctionService;
