const BANNER_CREATIVE_TYPE = 0;
const VIDEO_CREATIVE_TYPE = 1;
const NATIVE_CREATIVE_TYPE = 2;

const CLEANER_TIMEOUT_MS = 300000;

class OrdersManagementService {
    constructor() {
        this._bannerOrders = [];
        this._videoOrders = [];
        this._nativeOrders = [];
        this._lineItemSet = new Set();
        this._campainSet = new Set();
        this._targetingSet = new Set();
        this._cridToLineItemId = Object.create(null);
        this._cridToCampaignId = Object.create(null);
        this._cridToTargetingId = Object.create(null);

        this._cleanerTimeout = undefined;
    }

    setOrders(orders) {
        if (this._cleanerTimeout) {
            clearTimeout(this._cleanerTimeout);
        }

        this._bannerOrders = [];
        this._videoOrders = [];
        this._nativeOrders = [];
        this._lineItemSet.clear();
        this._campainSet.clear();
        this._targetingSet.clear();
        this._cridToLineItemId = Object.create(null);
        this._cridToCampaignId = Object.create(null);
        this._cridToTargetingId = Object.create(null);

        for (const order of orders) {
            // order.targeting = JSON.parse(order.targeting);
            if (order.cr_type === BANNER_CREATIVE_TYPE) {
                this._bannerOrders.push(order);
            }

            if (order.cr_type === VIDEO_CREATIVE_TYPE) {
                this._videoOrders.push(order);
            }

            if (order.cr_type === NATIVE_CREATIVE_TYPE) {
                this._nativeOrders.push(order);
            }

            this._lineItemSet.add(order.li_id);
            this._campainSet.add(order.c_id);
            this._targetingSet.add(order.targeting);
            this._cridToLineItemId[order.cr_id] = order.li_id;
            this._cridToCampaignId[order.cr_id] = order.c_id;
            this._cridToTargetingId[order.cr_id] = order.targeting_id;
        }

        this._cleanerTimeout = setTimeout(() => {
            this._bannerOrders = [];
            this._videoOrders = [];
            this._nativeOrders = [];
            this._lineItemSet.clear();
            this._campainSet.clear();
            this._targetingSet.clear();
            this._cridToLineItemId = Object.create(null);
            this._cridToCampaignId = Object.create(null);
            this._cridToTargetingId = Object.create(null);
        }, CLEANER_TIMEOUT_MS)
    }

    /**
     * @returns {Array}
     */
    getBannerBids() {
        return this._bannerOrders;
    }

    /**
     * @returns {Array}
     */
    getVideoOrders() {
        return this._videoOrders;
    }

    /**
     * @returns {Array}
     */
    getNativeOrders() {
        return this._nativeOrders;
    }

    /**
     * @param {number} crid
     * @returns {number}
     */
    getLineItemByCrid(crid) {
        return this._cridToLineItemId[crid];
    }

    /**
     * @param {number} crid
     * @returns {number}
     */
    getCampaignByCrid(crid) {
        return this._cridToCampaignId[crid];
    }

    /**
     * @param {number} crid
     * @returns {number}
     */
    getTargetingByCrid(crid) {
        return this._cridToTargetingId[crid];
    }

    /**
     * @returns {Set}
     */
    getLineItemSet() {
        return this._lineItemSet;
    }

    /**
     * @returns {Set}
     */
    getCampaingSet() {
        return this._campainSet;
    }

    /**
     * @returns {Set}
     */
    getTargetingSet() {
        return this._targetingSet;
    }
}

module.exports = OrdersManagementService;
