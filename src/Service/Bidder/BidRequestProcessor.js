'use strict';

const net = require('net');
const url = require('url');
const fastUrl = require('fast-url-parser');
const UAParser = require('ua-parser-js');

const {CountryCodesMatcher} = require('./Data/library.json');
const filters = require('../Filters');

const DEVICE_TYPE_UNKNOWN = 0;
const DEVICE_TYPE_SMARTPHONE = 1;
const DEVICE_TYPE_PC = 2;
const DEVICE_TYPE_TV = 3;
const DEVICE_TYPE_PHONE = 4;
const DEVICE_TYPE_TABLET = 5;
const DEVICE_TYPE_CONNECTED_DEVICE = 6;
const DEVICE_TYPE_SET_TOP_BOX = 7;

const PLACEMENT_TYPE_SITE = 0;
const PLACEMENT_TYPE_APP = 1;

const IMP_TYPE_BANNER = 0;
const IMP_TYPE_VIDEO = 1;
const IMP_TYPE_NATIVE = 2;

class BidRequestProcessor {
    constructor(options, geoLocationService) {
        this._markup = options.markup;
        this._geoLocationService = geoLocationService;
    }

    static isValidBidRequest(bidRequest) {
        if (bidRequest.id && Array.isArray(bidRequest.imp) && bidRequest.device && bidRequest.device.ua
            && net.isIPv4(bidRequest.device.ip)
        ) {
            if (bidRequest.site) {
                if (bidRequest.site.id && bidRequest.site.domain) {
                    return true;
                }
            } else if (bidRequest.app && bidRequest.app.id && bidRequest.app.name) {
                return true;
            }
        }

        return false;
    }

    static isValidImpObject(impObject) {
        return impObject.id && Number.isFinite(impObject.bidfloor)
            && !(impObject.bidfloorcur && impObject.bidfloorcur !== 'USD');
    }

    static getImpressionType(impObject) {
        if (impObject.banner) {
            return IMP_TYPE_BANNER;
        }

        if (impObject.video) {
            return IMP_TYPE_VIDEO;
        }

        if (impObject.native) {
            return IMP_TYPE_NATIVE;
        }

        return undefined;
    }

    findAcceptableBids(impType, impObject, bids) {
        return bids.filter(bid => {
            const exchangePrice = impObject.bidfloor;
            const orderMinPriceBoundary = bid.li_min_price || bid.c_max_price;
            const orderMaxPriceBoundary = bid.li_max_price || bid.c_max_price;
            const orderPrice = orderMaxPriceBoundary * (1 - this._markup);

            if (exchangePrice < orderMinPriceBoundary && exchangePrice > orderPrice) {
                return false;
            }

            if (impObject.secure && !bid.secure) {
                return false;
            }

            // 1 = the ad is interstitial or full screen, 0 = not interstitial
            if (!impObject.instl && bid.instl) {
                return false;
            }

            // // Ad position on screen
            // if (!Filters.adPositionFilter(impObject[impType].pos, bid.targeting.pos)) {
            //     return false;
            // }

            if (impObject[impType].w && impObject[impType].w !== bid.width) {
                return false;
            }

            if (impObject[impType].h && impObject[impType].h !== bid.height) {
                return false;
            }

            if (Array.isArray(impObject[impType].format)) {
                let matchFormat = false;

                for (const format of impObject[impType].format) {
                    if (format.w && format.w === bid.width && format.h && format.h === bid.height) {
                        matchFormat = true;
                        break;
                    }
                }

                return matchFormat;
            }

            return true;
        });
    }

    filtersAndOrderBidInfo(bidRequest, ssp, bidsMap) {
        // key => targeting_id; value => bool
        const targetingResult = Object.create(null);

        let inventoryInfo;
        let deviceInfo;
        let geoInfo;

        const filteredBids = Object.create(null);

        for (const impId in bidsMap) {
            const bids = bidsMap[impId];
            for (const bid of bids) {
                if (targetingResult[bid.targeting_id] === false) {
                    continue;
                }

                if (targetingResult[bid.targeting_id] === true) {
                    if (filteredBids[impId] === undefined) {
                        filteredBids[impId] = [bid];
                    } else {
                        filteredBids[impId].push(bid);
                    }

                    continue;
                }

                if (bidRequest.wseat && !filters.advertisersFilter(bid.adv_id, bidRequest.wseat, true)
                    || bidRequest.bseat && !filters.advertisersFilter(bid.adv_id, bidRequest.bseat, false)
                ) {
                    continue;
                }

                const targeting = bid.targeting;

                // SSP list
                if (targeting.ssp_partners && !filters.sspFilter(ssp, targeting.ssp_partners)) {
                    targetingResult[bid.targeting_id] = false;
                    continue;
                }

                if (inventoryInfo === undefined) {
                    inventoryInfo = this._getInventoryInfo(bidRequest.site, bidRequest.app);
                }

                if (deviceInfo === undefined) {
                    deviceInfo = this._getDeviceInfo(bidRequest.device, inventoryInfo.placementType);
                }

                if (geoInfo === undefined) {
                    geoInfo = this._getGeoInfo(bidRequest.device.geo, bidRequest.device.ip);
                }

                if (targeting.geo && targeting.geo.countries && !filters.countryFilter(geoInfo.country, targeting.geo.countries)) {
                    targetingResult[bid.targeting_id] = false;
                    continue;
                }

                targetingResult[bid.targeting_id] = true;

                if (!filters.bcatFilter(bid.cat, bidRequest.bcat)) {
                    continue;
                }

                if (filteredBids[impId] === undefined) {
                    filteredBids[impId] = [bid];
                } else {
                    filteredBids[impId].push(bid);
                }
            }
        }

        return {
            bidsByImpId: filteredBids,
            inventoryInfo,
            deviceInfo,
            geoInfo
        }
    }

    _getInventoryInfo(siteObj, appObj) {
        if (typeof siteObj === 'object') {
            let domain;

            if (typeof siteObj.domain === 'string' && siteObj.domain.length > 4) {
                domain = siteObj.domain
            } else if (typeof siteObj.page === 'string' && siteObj.page.length > 11) {
                domain = fastUrl.parse(siteObj.page).host;

                if (domain.length < 4) {
                    throw new Error('Can not detect domain of site');
                }
            } else {
                throw new Error('Can not detect domain of site');
            }

            domain = url.domainToASCII(domain);

            return {
                type: PLACEMENT_TYPE_SITE,
                id: siteObj.id || null,
                domain: domain,
                cat: siteObj.cat
            }
        } else {
            const name = url.domainToASCII(appObj.name);
            const domain = url.domainToASCII(appObj.domain);

            if (name.length < 2) {
                throw new Error('Can not detect name of app');
            }

            return {
                type: PLACEMENT_TYPE_APP,
                id: appObj.id || null,
                domain: domain || null,
                name: name,
                bundle: appObj.bundle || null,
                cat: appObj.cat
            }
        }
    }

    _getDeviceInfo(deviceObj, inventoryType) {
        const userAgentInfo = new UAParser(deviceObj.ua).getResult();

        const device = this._getDeviceType(userAgentInfo, inventoryType, deviceObj.devicetype);

        const brand = userAgentInfo.device.vendor && userAgentInfo.device.vendor.toLowerCase()
            || deviceObj.make && deviceObj.make.toLowerCase() || null;

        const model = userAgentInfo.device.model && userAgentInfo.device.model.toLowerCase()
            || deviceObj.model && deviceObj.model.toLowerCase() || null;

        const browser = userAgentInfo.browser.name && userAgentInfo.browser.name.toLowerCase() || null;

        const os = userAgentInfo.os.name && userAgentInfo.os.name.toLowerCase()
            || deviceObj.os && deviceObj.os.toLowerCase() || null;

        const connectionType = Number.isInteger(deviceObj.connectiontype) ? deviceObj.connectiontype : 0;

        return {
            device,
            brand,
            model,
            browser,
            os,
            ip: deviceObj.ip,
            lang: deviceObj.language && deviceObj.language.toLowerCase(),
            isp: deviceObj.carrier && deviceObj.carrier.toLowerCase(),
            connection: connectionType,
            ifa: deviceObj.ifa
        };
    }

    _getGeoInfo(geoObj, ip) {
        return {
            country: countryCodeToRtbStandard(geoObj.country) || null,
            region: geoObj.region && geoObj.region.toLowerCase() || null,
            city: geoObj.city || null
        };

        /**
        const geoInfo = this._geoLocationService.getLocation(ip);

        if (typeof geoInfo !== 'object') {
            throw new TypeError('Can not determine location info');
        }

        return {
            country: geoInfo.country.names.de, // "de": "USA" - ISO 3166-1 alpha-3
        }
        **/
    }

    /**
     * @param {{browser, engine, os, device}} uaInfo
     * @param {number} inventoryType
     * @param {*} sspDeviceType
     * @returns {number}
     * @private
     */
    _getDeviceType(uaInfo, inventoryType, sspDeviceType) {
        const uaDeviceTypeToInt = {
            console: DEVICE_TYPE_CONNECTED_DEVICE,
            mobile: DEVICE_TYPE_PHONE,
            tablet: DEVICE_TYPE_TABLET,
            smarttv: DEVICE_TYPE_TV
        };

        if (uaInfo.device.type === undefined && uaInfo.device.model === undefined) {
            if (inventoryType === PLACEMENT_TYPE_SITE) {
                return DEVICE_TYPE_PC;
            }
        }

        return uaDeviceTypeToInt[uaInfo.device.type]
            || Number.isInteger(sspDeviceType) && sspDeviceType
            || DEVICE_TYPE_UNKNOWN;
    }
}

function countryCodeToRtbStandard(isoCode) {
    const country = isoCode.toUpperCase();

    return country.length === 2 ? CountryCodesMatcher[country] : country;
}

module.exports = BidRequestProcessor;
