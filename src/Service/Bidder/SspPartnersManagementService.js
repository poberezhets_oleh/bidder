class SspPartnersManagementService {
    constructor() {
        this._sspByAccessKey = Object.create(null);
    }

    setPartners(partners) {
        for (const ssp of partners) {
            this._sspByAccessKey[ssp.ssp_key] = ssp;
        }
    }

    getSspPartner(accessKey) {
        return this._sspByAccessKey[accessKey];
    }
}

module.exports = SspPartnersManagementService;
