'use strict';

const winston = require('winston');

const {
    ConsoleTransport,
    FileTransport
} = require('./logger/transport');

class LoggerFactory {

    /**
     * @return {winston.Logger}
     */
    create(config) {
        const transports = [];

        Object.keys(config).forEach(transport => {
            const options = config[transport];

            if (transport === 'console') {
                transports.push(new ConsoleTransport(options).create());
            }

            if (transport === 'file') {
                transports.push(new FileTransport(options).create());
            }
        });

        return new winston.Logger({
            transports:  transports,
            exitOnError: false,
            emitErrs:    true
        });
    }
}

module.exports = LoggerFactory;
