'use strict';

const ConsoleTransport = require('./ConsoleTransport');
const FileTransport    = require('./FileTransport');

module.exports = {
    ConsoleTransport,
    FileTransport,
};
