'use strict';

const EventEmitter = require('events');
const _ = require('lodash');
const {BlockingQueryWatch} = require('blocking-query-client');
const {WatchError, AlreadyInitializedError} = require('./Error/index');

/**
 * @emits BlockingQueryWatcher#healthy
 * @emits BlockingQueryWatcher#unhealthy
 * @emits BlockingQueryWatcher#changed
 * @emits BlockingQueryWatcher#error
 */
class BlockingQueryWatcher extends EventEmitter {
    /**
     * @param {Object} options
     * @param {string} options.url
     * @param {number} [options.wait]
     * @throws {TypeError} On invalid options format
     * @public
     */
    constructor(options) {
        super();

        if (!_.isPlainObject(options)) {
            throw new TypeError('options must be an object');
        }

        if (!_.isString(options.url) || _.isEmpty(options.url)) {
            throw new TypeError('options.url must be non-empty string');
        }

        if (!_.isUndefined(options.wait) && (!Number.isSafeInteger(options.wait) || options.wait <= 0)) {
            throw new Error('options.wait must be a number greater then zero if set');
        }

        this._options = _.cloneDeep(options);
        this._initialized = false;

        this._onWatcherChange = this._onWatcherChange.bind(this);
        this._onWatcherError = this._onWatcherError.bind(this);
        this._onWatcherHealthy = this._onWatcherHealthy.bind(this);
        this._onWatcherUnhealthy = this._onWatcherUnhealthy.bind(this);

        this._blockingQueryWatcher = new BlockingQueryWatch(this._options.url, this._options.wait);
        this._setWatchUnhealthy();
        this._setUninitialized();
    }

    /**
     * @returns {string}
     */
    get CLASS_NAME() {
        return this.constructor.name;
    }

    /**
     * @returns {boolean}
     */
    isWatchHealthy() {
        return this._isWatchHealthy;
    }

    /**
     * @returns {boolean}
     */
    isInitialized() {
        return this._initialized;
    }

    _setWatchHealthy() {
        this._isWatchHealthy = true;
    }

    _setWatchUnhealthy() {
        this._isWatchHealthy = false;
    }

    _setInitialized() {
        this._initialized = true;
    }

    _setUninitialized() {
        this._initialized = false;
    }

    /**
     * Starts service and returns initial list of data.
     * Listens for changes after successful resolve.
     *
     * Rejection of promise means that watcher was stopped and no retries will be done.
     *
     * @returns {*} Data field of a response body
     * @throws {AlreadyInitializedError} If service is already started
     * @throws {WatchError} On Error from `blocking-query-client`
     * @public
     */
    async startService() {
        if (this.isInitialized()) {
            throw new AlreadyInitializedError('Service is already started');
        }

        let data;
        try {
            data = await this._blockingQueryWatcher.start();
        } catch (error) {
            throw new WatchError(`${this.CLASS_NAME} start was failed`, {error});
        }

        this._blockingQueryWatcher.on('changed', this._onWatcherChange);
        this._blockingQueryWatcher.on('error', this._onWatcherError);
        this._blockingQueryWatcher.on('healthy', this._onWatcherHealthy);
        this._blockingQueryWatcher.on('unhealthy', this._onWatcherUnhealthy);

        this._setInitialized();
        this._setWatchHealthy();

        return data;
    }

    /**
     * Stops service even if it is not started yet. Monitor becomes `uninitialized` and `unhealthy`.
     *
     * @returns {BlockingQueryWatcher}
     * @public
     */
    stopService() {
        if (!this.isInitialized()) {
            return this;
        }

        this._blockingQueryWatcher.removeAllListeners();
        this._blockingQueryWatcher.stop();
        this._blockingQueryWatcher = null;
        this._setUninitialized();
        this._setWatchUnhealthy();
        return this;
    }

    /**
     * This method receives data from response.
     *
     * If service was unhealthy, it becomes healthy.
     *
     * @param {{data: *, bq: {}}} body - response from blocking query
     * @emits OriginInfoMonitor#changed
     * @private
     */
    _onWatcherChange(body) {
        if (!this.isWatchHealthy()) {
            this._setWatchHealthy();
        }

        this.emit('changed', body);
    }

    _onWatcherError(error) {
        if (this.isWatchHealthy()) {
            this._setWatchUnhealthy();
        }

        this.emit('error', new WatchError(error.message, {error}));
    }

    _onWatcherHealthy() {
        this._setWatchHealthy();
        this.emit('healthy');
    }

    _onWatcherUnhealthy() {
        this._setWatchUnhealthy();
        this.emit('unhealthy');
    }
}

module.exports = BlockingQueryWatcher;
