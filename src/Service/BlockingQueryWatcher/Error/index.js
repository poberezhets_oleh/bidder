'use strict'

const ExtendableError = require('./ExtendableError');

class WatchError extends ExtendableError {}
class AlreadyInitializedError extends ExtendableError {}

module.exports = {
    WatchError,
    AlreadyInitializedError
};
