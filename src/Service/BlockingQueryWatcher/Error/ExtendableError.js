'use strict';

class ExtendableError extends Error {
    /**
     * @param {string} message
     * @param {*} extra
     */
    constructor(message = '', extra) {
        super(message);

        this.message = message;
        this.extra   = extra;
        this.name    = this.constructor.name;
        this.code    = 'ECUSTOM';

        // noinspection JSUnresolvedFunction
        Error.captureStackTrace(this, this.constructor);
    }

    /**
     * @return {string}
     */
    valueOf() {
        return !this.extra
            ? super.valueOf()
            : super.valueOf() + ' Extra: ' + JSON.stringify(this.extra);
    }
}

module.exports = ExtendableError;
