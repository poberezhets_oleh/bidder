'use strict';

const Aerospike = require('aerospike');

class AerospikeBidsCacheClient {
    /**
     * @param {Object} config
     * @param {aerospike.Client} aerospikeConnection
     */
    constructor(config, aerospikeConnection) {
        this._ns = config.namespace;
        this._set = config.set;
        this._asConnection = aerospikeConnection;
    }

    async setBid(key, data, ttl) {
        const asKey = new Aerospike.Key(this._ns, this._set, key);

        const meta = {ttl};
        const asData = Object.create(null);

        for (const key in data) {
            if (data[key] === null) continue;

            asData[key] = Number.isFinite(data[key]) && data[key] < 1 && data[key] > 0 ? new Aerospike.Double(data[key]) : data[key];
        }

        await this._asConnection.put(asKey, asData, meta);
    }
}

module.exports = AerospikeBidsCacheClient;
