'use strict'

const Fs = require('fs');
const path = require('path');
const Util = require('util');

const readDirAsync = Util.promisify(Fs.readdir);

/**
 * Return {httpMethod: {httpPath: method}}
 * @param {String} dirPath
 * @param {Object} app
 * @return {Object}
 */
module.exports = async (dirPath, app) => {
    let routes = Object.create(null);

    const files = await readDirAsync(dirPath);

    files.forEach(file => {
        const resource = require(path.join(dirPath, file))(app);

        Object.assign(routes, resource);
    });

    return routes;
}
