
exports.adPositionFilter = function (position, filterList) {
    if (filterList.length === 0) {
        return true;
    }

    return filterList.includes(position);
};

exports.advertisersFilter = function (advertiser, filterList, isWhiteList = true) {
    if (!Array.isArray(filterList) || filterList.length === 0) {
        return true;
    }

    return isWhiteList ? filterList.includes(advertiser) : !filterList.includes(advertiser);
};

exports.bcatFilter = function (bidCategories, blockedCategories) {
    if (!Array.isArray(blockedCategories) || blockedCategories.length === 0) {
        return true;
    }

    for (const bidCat of bidCategories) {
        if (blockedCategories.includes(bidCat)) {
            return false;
        }
    }

    return true;
};

exports.sspFilter = function (ssp, filterList) {
    if (filterList.length === 0) {
        return true;
    }

    return filterList.includes(ssp);
};

exports.deviceTypeFilter = function (deviceType, filterList) {
    if (filterList.length === 0) {
        return true;
    }

    return deviceType ? filterList.includes(deviceType) : false;
};

exports.languageFilter = function (language, filterList) {
    if (filterList.length === 0) {
        return true;
    }

    return language ? filterList.includes(language) : false;
};

exports.osFilter = function (os, filterList) {
    if (filterList.length === 0) {
        return true;
    }

    return os ? filterList.includes(os) : false;
};

exports.browserFilter = function (browser, filterList) {
    if (filterList.length === 0) {
        return true;
    }

    return browser ? filterList.includes(browser) : false;
};

exports.connectionTypeFilter = function (connectionType, filterList) {
    if (filterList.length === 0) {
        return true;
    }

    return connectionType ? filterList.includes(connectionType) : false;
};

exports.ispFilter = function (isp, filterList) {
    if (filterList.length === 0) {
        return true;
    }

    return isp ? filterList.includes(isp) : false;
};

exports.countryFilter = function (country, filterList, isWhiteList = true) {
    if (filterList.length === 0) {
        return true;
    }

    if (!country) {
        return false;
    }

    if (isWhiteList) {
        return filterList.includes(country);
    } else {
        return !filterList.includes(country);
    }
};

exports.regionFilter = function (region, filterList, isWhiteList = true) {
    if (filterList.length === 0) {
        return true;
    }

    if (!region) {
        return false;
    }

    if (isWhiteList) {
        return filterList.includes(region);
    } else {
        return !filterList.includes(region);
    }
};

exports.cityFilter = function (city, filterList, isWhiteList = true) {
    if (filterList.length === 0) {
        return true;
    }

    if (!city) {
        return false;
    }

    if (isWhiteList) {
        return filterList.includes(city);
    } else {
        return !filterList.includes(city);
    }
};

