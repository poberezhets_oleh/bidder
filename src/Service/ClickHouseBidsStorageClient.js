'use strict';

const EventEmitter = require('events');

class ClickHouseBidsStorageClient extends EventEmitter{
    /**
     * @param {{}} config
     * @param {ClickHouse} clickHouseCli
     */
    constructor(config, clickHouseCli) {
        super();

        this._config = config;
        this._database = config.database;
        this._tablePrefix = config.table;
        this._clickHouseCli = clickHouseCli;

        this._rows = [];
        this._tmpRows = [];
        this._insertProcessing = false;

        setInterval(() => this._saveRows(), 10000);
    }

    /**
     * @param {{}} recordData
     */
    saveRecord(recordData) {
        const dateObj = new Date();
        const date = dateObj.toISOString().substr(0, 10);
        const time = `${date} ${dateObj.getUTCHours()}:00:00`;

        const row = Object.assign({date, time}, recordData);

        if (this._insertProcessing) {
            this._tmpRows.push(row);
        } else {
            this._rows.push(row);
        }
    }

    async _saveRows() {
        this._rows.push(...this._tmpRows);
        this._tmpRows = [];

        if (this._rows.length > 0) {
            this._insertProcessing = true;

            try {
                this._clickHouseCli.sessionId = Date.now();

                await this._clickHouseCli.query(`USE ${this._database}`).toPromise();

                const month = this._rows[0].date.substr(0, 7).replace('-', '_');

                const query = `INSERT INTO ${this._tablePrefix}_${month} (`
                    + 'creative, line_item, campaign, advertiser, ssp, ssp_price, bid_price, crid_price,'
                    + 'country, region, city, placement, placement_id, domain, app_name, bundle, browser,'
                    + 'ip, os, isp, connection, lang, device, brand, model, time, date'
                    + ')';

                await this._clickHouseCli.insert(query, this._rows).toPromise();

                this._clickHouseCli.sessionId = null;
            } catch (error) {
                this.emit('error', error);
            }

            this._insertProcessing = false;
            this._rows = [];
        }
    }
}

module.exports = ClickHouseBidsStorageClient;
