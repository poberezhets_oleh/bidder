'use strict';

const {ClickHouse} = require('clickhouse');

const Bidder = require('./src/Bidder');
const AerospikeBidsCacheClient = require('./src/Service/AerospikeBidsCacheClient');
const ClickHouseBidsStorageClient = require('./src/Service/ClickHouseBidsStorageClient');
const LoggerFactory = require('./src/Service/LoggerFactory');
const {errorFormatter} = require('./src/helper/formatter');

let logger;
let bidderApp;

async function startApplication(config, aerospikeClient) {
    if (typeof global.gc === 'function') {
        setInterval(global.gc, config.gcInterval);
    }

    logger = new LoggerFactory().create(config.logger);

    logger.on('error', (err, transport) => {
        const transportName = (transport && transport.name) ? transport.name : 'unnamedTransport';
        const formattedTransportError = errorFormatter(`${transportName} error`, err);

        console.log(formattedTransportError.formattedMessage, formattedTransportError.formattedException);
    });

    const aerospikeConnection = await aerospikeClient.connect();
    const clickHouseCli = new ClickHouse(config.clickHouse);

    const cacheClient = new AerospikeBidsCacheClient(config.bidsCache, aerospikeConnection);
    const storageClient = new ClickHouseBidsStorageClient(config.bidsStorage, clickHouseCli);

    bidderApp = new Bidder(config.bidder, logger, cacheClient, storageClient);

    await bidderApp.bootstrap();
    await bidderApp.start();

    process.on('message', message => bidderApp.processMessage(message));

    logger.info(`Bidder Application started with pid: ${process.pid}`);
}

async function stopApplication(emergency = false) {
    if (!bidderApp) {
        process.exit(0);
        return;
    }

    try {
        await bidderApp.stop();
        logger.info(`Bidder Application stopped with pid: ${process.pid}`);
    } catch (err) {
        console.log(err);

        logger.error(`Bidder Application has failed to stop, error = ${err}`, err);
        emergency = true;
    }

    try {
        logger.close();

        setTimeout(() => process.exit(emergency ? 1 : 0), 5000);
    } catch (err) {
        const formattedLoggerErr = errorFormatter('Logger Error', err);

        console.log(formattedLoggerErr.formattedMessage, formattedLoggerErr.formattedException);

        setTimeout(() => process.exit(emergency ? 1 : 0), 5000);
    }
}

// startApplication().catch(err => {
//     logger.Error(err.message, err);
//     logger.close();
//
//     setTimeout(() => process.exit(1), 1000);
// });

process.on('unhandledRejection', reason => {
    logger.error('unhandledRejection' + (reason.message || reason), reason);
});

process.on('uncaughtException', uncaughtException => {
    const formattedUncaughtException = errorFormatter('uncaughtException', uncaughtException);

    console.log(formattedUncaughtException.message, formattedUncaughtException.formattedException);
    logger.error(formattedUncaughtException.message, uncaughtException, transportError => {
        if (transportError) {
            const formattedTransportError = errorFormatter('transportError', transportError);

            console.log(formattedTransportError.formattedMessage, formattedTransportError.formattedException);
        }

        // close connections, stop Service and after that call logger.close()
        logger.close();

        setTimeout(() => process.exit(1), 1000);
    });
});

module.exports = {
    startApplication,
    stopApplication
};
