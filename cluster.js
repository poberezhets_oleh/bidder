'use strict';

const os = require('os');
const cluster = require('cluster');
const childProcess = require('child_process');

const aerospike = require('aerospike');
const config = require('config');
const bidderApp = require('./bidderApp');

let defaults = {
    socketTimeout: 500,
    totalTimeout: 1000,
    exists: aerospike.policy.exists.CREATE_OR_REPLACE
};

const aerospikeConfig = {
    hosts: config.aerospike.hosts.reduce((a, c) => a + (a === '' ? '' : ',') + `${c}:${config.aerospike.port}`, ''),
    connTimeoutMs: 1000,
    log: {
        level: aerospike.log[config.aerospike.log.level],
        file: config.aerospike.log.file
    },
    policies: {
        write: new aerospike.WritePolicy(defaults)
    },
    sharedMemory: {
        key: 0xa5000000
    }
};

const aerospikeClient = aerospike.client(aerospikeConfig);


cluster.on('exit', (worker, code, signal) => {
    if (worker.exitedAfterDisconnect === true) {
        console.log('Oh, it was just voluntary – no need to worry', {workerId: worker.id, code, signal});
    } else {
        console.error('Worker exited without disconnect', {workerId: worker.id, code, signal});
    }
});

// const workerNumber = os.cpus().length;
const workerNumber = 1;

const workersList = [];
let bidderCcWorker;

if (cluster.isMaster) {
    process.on('SIGTERM', stopCluster);
    process.on('SIGINT', stopCluster);

    process.on('disconnect', () => {
        console.log('Master disconnect');
    });

    process.on('exit', () => {
        console.log('Master exit ' + process.pid);
    });

    forkWorkers(workerNumber)
        .then(() => {
            eachWorker(worker => {
                worker.on('error', error => {
                    console.error(error);
                });
            });

            cluster.setupMaster({
                exec: 'bidderCcApp.js'
            });

            bidderCcWorker = cluster.fork();

            bidderCcWorker.on('message', data => {
                eachWorker(worker => {
                    worker.send(data);
                });
            });
        })
        .catch(error => {
            console.error(error);
        });

} else {
    process.on('SIGTERM', bidderApp.stopApplication);
    process.on('SIGINT', bidderApp.stopApplication);

    bidderApp.startApplication(config, aerospikeClient)
        .catch(err => {
            if (err instanceof aerospike.AerospikeError && err.isServerError) {
                console.error(`Aerospike server error: ${err.message}`);
            }

            console.error(err);
        });
}

function stopCluster() {
    console.log('stopCluster');

    eachWorker(worker => {
        worker.disconnect();
    });

    if (bidderCcWorker) {
        bidderCcWorker.disconnect();
    }

    aerospikeClient.close();
}

function eachWorker(callback) {
    for (const worker of workersList) {
        callback(worker);
    }
}

/**
 * @param {number} workerNumber
 * @return {Promise}
 */
function forkWorkers(workerNumber) {
    let forksCount = 0;

    return new Promise((resolve, reject) => {
        for (let i = 0; i < workerNumber; i++) {
            const worker = cluster.fork();

            worker.once('listening', () => {
                console.log('listening ' + worker.id);
                forksCount++;

                workersList.push(worker);

                if (forksCount === workerNumber) {
                    resolve();
                }
            });

            worker.once('error', error => {
                reject(error);
            });
        }
    });
}
