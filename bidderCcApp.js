'use strict';

const consul = require('consul');
const config = require('config');

const LoggerFactory = require('./src/Service/LoggerFactory');
const {errorFormatter} = require('./src/helper/formatter');

const ControlCenter = require('./src/ControlCenter');

const logger = new LoggerFactory().create(config.logger);
const consulCli = consul(config.consul);

const bidderCcApp = new ControlCenter(config.bidderCc, logger, consulCli);

async function startApplication() {
    await bidderCcApp.bootstrap();
    await bidderCcApp.start();

    logger.info(`Bidder Control Center Application started with pid: ${process.pid}`);
}

let stopping = false;

async function stopApplication(emergency = false) {
    if (stopping) {
        return;
    }

    stopping = true;

    try {
        await bidderCcApp.stop();
        logger.info(`Bidder Control Center Application stopped with pid1: ${process.pid}`);
    } catch (err) {
        console.log(err);

        logger.error(`Bidder Control Center Application has failed to stop, error = ${err}`, err);
        emergency = true;
    }

    try {
        logger.close();

        setTimeout(() => process.exit(emergency ? 1 : 0), 1000);
    } catch (err) {
        const formattedLoggerErr = errorFormatter('Logger Error', err);

        console.log(formattedLoggerErr.formattedMessage, formattedLoggerErr.formattedException);

        setTimeout(() => process.exit(emergency ? 1 : 0), 1000);
    }
}

startApplication().catch(err => {
    try {
        logger.error(err);
        logger.close();
    } catch (loggerErr) {
        const formattedLoggerErr = errorFormatter('Logger error', loggerErr);

        console.log(formattedLoggerErr.formattedMessage, formattedLoggerErr.formattedException);
    } finally {
        setTimeout(() => process.exit(1), 1000);
    }
});

process.on('SIGTERM', stopApplication);
process.on('SIGINT', stopApplication);

process.on('unhandledRejection', reason => {
    logger.error('unhandledRejection' + (reason.message || reason), reason);
});

process.on('uncaughtException', uncaughtException => {
    const formattedUncaughtException = errorFormatter('uncaughtException', uncaughtException);

    console.log(formattedUncaughtException.message, formattedUncaughtException.formattedException);
    logger.error(formattedUncaughtException.message, uncaughtException, transportError => {
        if (transportError) {
            const formattedTransportError = errorFormatter('transportError', transportError);

            console.log(formattedTransportError.formattedMessage, formattedTransportError.formattedException);
        }

        // close connections, stop Service and after that call logger.close()
        logger.close();

        setTimeout(() => process.exit(1), 1000);
    });
});

logger.on('error', (err, transport) => {
    const transportName = (transport && transport.name) ? transport.name : 'unnamedTransport';
    const formattedTransportError = errorFormatter(`${transportName} error`, err);

    console.log(formattedTransportError.formattedMessage, formattedTransportError.formattedException);
});

