const config = require('config');

const { ClickHouse } = require('clickhouse');

const client = new ClickHouse(config.clickHouse);

const month = new Date().toISOString().substr(0, 7).replace('-', '_');

const queries = [
    (`CREATE TABLE ${config.bidsStorage.database}.${config.bidsStorage.table}_${month}
        (
            creative INTEGER,
            line_item INTEGER,
            campaign INTEGER,
            advertiser INTEGER,
            ssp INTEGER,
            ssp_price DOUBLE,
            bid_price DOUBLE,
            crid_price DOUBLE,
            country VARCHAR,
            region Nullable(VARCHAR),
            city Nullable(VARCHAR),
            placement INTEGER,
            placement_id Nullable(VARCHAR),
            domain VARCHAR,
            app_name Nullable(VARCHAR),
            bundle Nullable(VARCHAR),
            browser Nullable(VARCHAR),
            ip VARCHAR,
            os Nullable(VARCHAR),
            isp Nullable(VARCHAR),
            connection Nullable(INTEGER),
            lang Nullable(VARCHAR),
            device INTEGER,
            brand Nullable(VARCHAR),
            model Nullable(VARCHAR),
            time DateTime,
            date Date
        ) 
        ENGINE=MergeTree(date, (creative, campaign, time), 8192)`).replace(/\n\s+/gm, ''),
];


setTimeout(async () => {
    for (const query of queries) {
        await client.query(query).toPromise();
    }
}, 0);
